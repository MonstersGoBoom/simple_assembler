
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>

typedef struct {
	char name[32];
	int opcode;
	int nbytes;
} OP;

OP oplist[256];

bool CMD_org(char *buffer);
bool CMD_defbin(char *buffer);
bool CMD_defbinw(char *buffer);
bool CMD_defbinl(char *buffer);
bool CMD_incbin(char *buffer);

typedef struct 
{
	char *name;
	bool (*func)(char *);
} Commands;

Commands command_list[]=
{
	{".org",CMD_org},
	{".incbin",CMD_incbin},
	{".db",CMD_defbin},
	{".dw",CMD_defbinw},
	{".dl",CMD_defbinl},
	{NULL,NULL}
};

char dstring[256];
uint16_t start_address=0;
uint16_t pc = 0;
uint8_t rom[65536];

// load a file and allocate the buffer for it 
unsigned char *loadFile(const char *name,uint64_t *size)
{
FILE    *fp = fopen(name,"rb");
unsigned int len;
unsigned char *buffer;

	if (fp==NULL)
	{
		printf("error opening %s\n",name);
		return NULL;
	}

	fseek(fp,0,SEEK_END);
	len = ftell(fp);
	fseek(fp,0,SEEK_SET);

	buffer = (unsigned char*)malloc(len+1);
	if (buffer == NULL)
	{
		printf("error allocating %x bytes\n",len);
		exit(-1);
	}
	buffer[len] = 0;
	fread(buffer,len,1,fp);
	*size = len;
	return (buffer);
}

void readOpcodeList(const char *fname)
{
	memset(oplist,0,sizeof(oplist));
	FILE *fp=fopen(fname,"rt");
	if (fp==NULL)
	{
		printf("opcode list not loaded\n");
		exit(0);
	}
	//	read each line of the opcode list file 
	int oindex = 0;
	while(!feof(fp)) {
		char string[256];
		if (fgets(string,256,fp)!=NULL)	{
			//	name,opcode
			char *opn = strtok (string,"=-\n\r\t");
			char *opc = strtok(NULL,"=-\n\r\t");
			if (opn!=NULL) {
				int opcode = strtol (opc,NULL,16);
				if (opn!=NULL) {
					OP *op=&oplist[oindex++];
					op->nbytes=1;
					if ((opcode>0xff) && (opcode<=0xffff))
						op->nbytes=2;

					sprintf(op->name,"%s\0",opn);
					op->opcode = opcode;
				}
			}
		}
	}
	fclose(fp);
}

//	decode a single line
bool decodeInstruction(char *in,char *target,int *val,int *bytesout) {
char *p;
char *o;
int sizeofnumber;
char work[32];
int q;
int index;
int value;
	//	we have a place to store bytesout
	if (bytesout!=NULL)	*bytesout=0;
	//	clear the buffer
	memset(work,0,sizeof(work));
	p=in;
	o=target;
	//	compare the description line against the input line
	//	we remove whitespace 
	//	so 
	//	lda #$00 -> lda#$00
	//	we then check against the target strings ( from .lst file )
	//	the target can state it's expectations
	//	eg. "adc#@1=69" expects a single byte @1 
	//		"asl@2,x" expects two bytes ( and ,x after it)

	while (*p!=0) {
		//	only compare none whitespace
		if (isspace(*p)==false) {
			//	if it's different , lets see why 
			if (*o!=*p) {
				if (*o=='@') {
					//	is it a number request ?
					//	process it 
					value = 0;
					o++;
					work[0]=*o++;
					sizeofnumber=atoi(work);

					if (bytesout!=NULL)	*bytesout=sizeofnumber;
					//	how big is the number
					if (sizeofnumber==1)		sizeofnumber=0xff;
					else if (sizeofnumber==2) 	sizeofnumber=0xffff;
					else if (sizeofnumber==4) 	sizeofnumber=0xffffffff;
					//	is it a hex number ?
					if(*p=='$') {
						p++;
						index = 0;
						for (q=0;q<sizeof(work);q++) {
							work[index++]=*p++;
							if (isxdigit(*p)==false) {
								break;
							}
						}
						work[index++]=0;
						value = strtol (work,NULL,16);
					} else if(*p=='%') { // or a binary number
						p++;
						index = 0;
						for (q=0;q<sizeof(work);q++) {
							work[index++]=*p++;
							if ((*p!='0') && (*p!='1')) {
								break;
							}
						}
						work[index++]=0;
						value = strtol(work,NULL,2);
					} else {	// or base 10 
						index = 0;
						for (q=0;q<sizeof(work);q++) {
							work[index++]=*p++;
							if (isdigit(*p)==false) {
								break;
							}
						}
						work[index++]=0;
						//	fix for labels containing partial keywords
						for (q=0;q<strlen(work);q++) {
							if (isdigit(work[q])==false)
							{
								// 	check for a label ?
								//	check for constants
								return false;
							}
						}
						value = strtol(work,NULL,10);
					}
					//	check against the size of number expected
					if (value<=sizeofnumber)  {
						//	ok we can use it
						if (val!=NULL)
							*val=value;
					} else {
						//	it doesn't match so fail
						return false;
					}
				} else {
					//	it's not a number request so it's different ( error )
					return false;
				}
			}
			o++;
		}
		p++;
	}
	return true;
}

bool checkOpcodes(char *input)
{
	int digits=0;
	int bout;

	for (int q=0;q<256;q++)
	{
		if (oplist[q].name==NULL) continue;

		if (decodeInstruction(input,oplist[q].name,&digits,&bout)==true) {
			if (oplist[q].nbytes==1)
				rom[pc++]=oplist[q].opcode;
			if (oplist[q].nbytes==2)
			{
				rom[pc++]=oplist[q].opcode&0xff;
				rom[pc++]=oplist[q].opcode>>8;
			}
			//	encode the number as it's expected 
			switch(bout)
			{
				case 1:	rom[pc++]=digits;;
								break;
				case 2:	
				{
					rom[pc++]=digits&0xff;
					rom[pc++]=digits>>8;
				}
				break;
			}
			return true;
			break;
		}
	}
	return false;
}


bool CMD_org(char *buffer)
{
int norg;	
char *ptr=buffer;
	while(isspace(*ptr++)==true) {}
	if (*ptr=='$')
	{
		norg = strtol(ptr+1,NULL,16);
	}
	else 
		norg = strtol(ptr,NULL,10);

	if (start_address==0)
		start_address=norg;
	pc = norg;
}

char *QuotedString(char *buffer)
{
char *p = buffer;	
char *d=&dstring[0];
	while(1)
	{
		if (*p==0) break;
		if (*p=='\n') break;
		if (*p=='\r') break;
		if (!isspace(*p) && (*p!='\"'))
		{
			*d++=*p;
		}
		p++;
	}
	return dstring;
}

bool CMD_defbin(char *buffer)
{
char *pch = strtok (buffer," ,\t\r\n");
uint8_t b;
	while(pch!=NULL)
	{
		b=strtol(pch,NULL,16);
		rom[pc++]=b;
		pch=strtok(NULL," ,\t\n\r");
	}
	return true;
}

bool CMD_defbinw(char *buffer)
{
char *pch = strtok (buffer," ,\t\r\n");
uint16_t b;
	while(pch!=NULL)
	{
		b=strtol(pch,NULL,16);
		rom[pc++]=b&0xff;
		rom[pc++]=b>>8;
		pch=strtok(NULL," ,\t\n\r");
	}
	return true;
}

bool CMD_defbinl(char *buffer)
{
char *pch = strtok (buffer," ,\t\r\n");
uint32_t b;
	while(pch!=NULL)
	{
		b=strtol(pch,NULL,16);
		rom[pc++]=b&0xff;
		rom[pc++]=b>>8;
		rom[pc++]=b>>16;
		rom[pc++]=b>>24;
		pch=strtok(NULL," ,\t\n\r");
	}
	return true;
}

bool CMD_incbin(char *buffer)
{
FILE *fbin;	
uint8_t b;
	char *name=QuotedString(buffer);
	fbin = fopen(name,"rb");
	if (fbin!=NULL)
	{
		while(!feof(fbin))
		{
			b=fgetc(fbin);
			rom[pc++]=fgetc(fbin);
		}
		fclose(fbin);
	}
	else 
		printf(">%s< not found\n",name);
}

bool checkCommand(char *input)
{
int q;
	for (q=0;command_list[q].name!=NULL;q++)	
	{
		char *cmd = strstr(input,command_list[q].name);
		if (cmd!=NULL)
		{
			command_list[q].func(cmd+strlen(command_list[q].name));
			return true;
		}
	}
	return false;
}

void Asm(char *ptr)
{
	bool validline;
	char input[256];
	int q =0;
	memset(input,0,sizeof(input));
	pc = 0;
	while(*ptr!=0) {
		if (*ptr=='\n') {	
			validline=false;
			if (strlen(input)>=3)
			{
				for (q=0;q<strlen(input);q++)
				{
					if (input[q]==';')
					{
						input[q]=0;
						break;
					}
				}
				if (checkCommand(input)==true)
				{
				}	else if (checkOpcodes(input)==true) {
				}
				else {
					printf("\ndon't know [%s]\n",input);
				}
			}
			memset(input,0,sizeof(input));
			q=0;
		}
		else 
		{
			input[q++]=*ptr;
		}
		ptr++;
	}
}

void main(int argc,char *argv[])
{
	uint64_t size;
	//	not super sexy 

	if (argc<3)
	{
		printf("asm <cpu> <filename>\n");
		printf("	z80\n");
		printf("	68k\n");
		printf("	6502\n");
		return;
	}

	if (stricmp(argv[1],"z80")==0)
		readOpcodeList("mz80.lst");
	else if (stricmp(argv[1],"68k")==0)
		readOpcodeList("m68000.lst");
	else
		readOpcodeList("m6502.lst");

	char *file = loadFile(argv[2],&size);

	Asm(file);

	printf("saving rom %d bytes\n",pc-start_address);
	FILE *fp=fopen("rom.bin","wb");
	fwrite(&rom[start_address],pc-start_address,1,fp);
	fclose(fp);
#if 0
	fp=fopen("rom.prg","wb");
	fwrite(&start_address,2,1,fp);
	fwrite(&rom[start_address],pc-start_address,1,fp);
	fclose(fp);
#endif
	free(file);
}

